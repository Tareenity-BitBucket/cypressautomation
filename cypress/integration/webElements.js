/// <reference types="cypress" />
class elements {

    // Preprod URL 
    preprodURL(){
        return cy.visit('https://www.preprod.arrivabus.co.uk/')
    }

    // Consent Form
    consentPopup(){
        return cy.get('#consent_prompt_submit')
    }

    /*<------------------ Journey Search ------------------>*/
    // From Search Box
    fromStation(){
        return cy.get('#react-select-3-input')
    }

    // To Search Box
    toStation(){
        return cy.get('#react-select-4-input')
    }

    // From / To Dropdown item list
    Dropdown(){
        return cy.get('.search-select__menu-list.css-11unzgr > div')
    }

    // Text Box containing the selected Stations
    selectedStations(){
        return cy.get('.search-select__single-value.css-1uccc91-singleValue')
    }

    // Getting the Date Picker Text Box
    datePicker(){
        return cy.get('._2SEQYXPF0ZUk74ybGirxc4')
    }

        // Clicking the Done Button from the dropwdown Text Box
        datePickerDone(){
            return cy.get('._2nLcqhYfJi2Jep-CiHF4mi > ._nfmPejYIgKBBPi4ONjx7')
        }

    // Date and Time Text Box
    dateAndTime(){
        return cy.get('#DPdefault')
    }
    // "Plan My Journey" Submission CTA
    planMyJourneyCTA(){
        return cy.get('#button-plan-my-journey')
    }

    /*<------------------ ^^Journey Search ------------------>*/

    /*<------------------ Plan my Journey ------------------>*/

    // Map
    planMyJourneyMap(){
        return cy.get('._3JdDtRzs6slkc4EZfHS8MR')
    }

    // Item in the Journey List
    journeyListItems(){
        return cy.get('._1Ls7L4IG3SdzfnJApyxYzG.undefined')
    }

    // Later Buses Icon
    laterBuses(){
        return cy.get('._1HLbyfas793NJHm8vXSbJ5.undefined')
    }

    // Buy Tickets CTA
    buyTicketsCTA(){
        return cy.get('._nfmPejYIgKBBPi4ONjx7')
    }

    // Selecting the Adult Section
    adultSection(){
        return cy.get('._1dP3L0IGJNdhuKwfztirAZ').eq(0)
    }
    
    // Adding an Adult Ticket
    adultTicket(){
        return cy.get('._1DebFuQJHF4VuR7Tx7YtGY').eq(1)
    }

    // "More Tickets" CTA
    moreTickets(){
        return cy.get('._2UBNoRINUEoQwUEFytkKOd > ._nfmPejYIgKBBPi4ONjx7')
    }

    // Child Section
    childSection(){
        return cy.get('._1dP3L0IGJNdhuKwfztirAZ').eq(1)
    }

    // Child Week Ticket
    childWeekTicket(){
        return cy.get('._1DebFuQJHF4VuR7Tx7YtGY').eq(1)
    }

    // Checkout CTA
    checkoutCTA(){
        return cy.get('._nfmPejYIgKBBPi4ONjx7')
    }

    /*<------------------ ^^ End of Plan my Journey ------------------>*/

    /*<------------------ Sign In Flow ------------------>*/

    // "Sign In" CTA
    signInCTA(){
        return cy.get('[data-elid="sign-in-button"]')
    }

    // Sign in Email
    signInEmail(){
        return cy.get('[data-elid="email-input"]')
    }

    // Sign in Password
    signInPassword(){
        return cy.get('[data-elid="password-input"]')
    }

    // "Continue" CTA
    signInContinueCTA(){
        return cy.get('._nfmPejYIgKBBPi4ONjx7._1f9IOXUvoBtQpEcftQIMP')
    }

    /*<------------------ ^^End of Sign In Flow ------------------>*/

    /*<------------------ Payment Flow ------------------>*/

    // "Choose Another way to pay" Link
    cardAlreadyAdded(){
        return cy.get('[data-braintree-id="toggle"] > span')
    }

    // Choosing "Card" as the payment option
    cardPaymentOption(){
        return cy.get('.braintree-option.braintree-option__card')
    }

    // Credit Card Number Field
    creditCardNum(){
        return cy.iframe('#braintree-hosted-field-number').find('#credit-card-number')
    }

    // Expiration Date Field
    creditCardexpirationDate(){
        return cy.iframe('#braintree-hosted-field-expirationDate').find('#expiration')
    }

    // Credit Card CVV Field
    creditCardCVV(){
        return cy.iframe('#braintree-hosted-field-cvv').find('#cvv')
    }

    // Credit Card Postal Field
    creditCardPostal(){
        return cy.iframe('#braintree-hosted-field-postalCode').find('#postal-code')
    }

    // "Confirm Payment" CTA
    confirmPaymentCTA(){
        return cy.get('#submit-payment-button')
    }

    // OTP Flow
    OTPNum(){
        return cy.iframe('#Cardinal-CCA-IFrame').find('.input-field')
    }

    // OTP Submit CTA
    OTPSubmitCTA(){
        return cy.iframe('#Cardinal-CCA-IFrame').find('.button.primary')
    }

    /*<------------------ ^^End of Payment Flow ------------------>*/

    /*<------------------ Ticket Catalogue Flow ------------------>*/
    // Tickets Heading in the Nav Bar
    ticketsHeading(){
        return cy.get('[data-elid="Tickets"]')
    }

    // Clicking the "Buy Tickets" navbar section
    buyTicketsHeading(){
        return cy.get('[data-elid="Buy tickets"]')
    }

    // items in the Region List
    regionList(){
        return cy.get('._1iiweaVLbU5Eo1UOZRpH_N')
    }

    // Selecting a Region
    regionSelection(){
        return cy.get('._1iiweaVLbU5Eo1UOZRpH_N > a')
    }

    // Region Heading
    regionHeading(){
        return cy.get('._1dLfQ8pPETO_iURJ0eN9c6')
    }

    // Zone List Check
    zoneList(){
        return cy.get('.jyivLROvupXFl2MmbNb__')
    }

    // Back Icon (To the Regions Page)
    backToRegionsPage(){
        return cy.get('._61Zg4qnEv7zhD5cWQ2uuB')
    }

    // Selecting a Zone
    zoneSelection(){
        return cy.get('._1oPO-NACXn6luIToRgUIef')
    }

    // Ticket Catalogue Checkout CTA
    ticketCheckoutCTA(){
        return cy.get('[data-elid="navigate-to-checkout-button"]')
    }

    /*<------------------ ^^End of Ticket Catalogue Flow ------------------>*/


    /*<------------------ Account Registration ------------------>*/
    // Sign In CTA in the Nav Bar
    signInNav(){
        return cy.get('[data-elid="top-line-account-not-logged"]')
    }

    // Create Account Tab on the Sign in page
    createAccountTab(){
        return cy.get('[data-elid="sign-up-tab"]').click()
    }

    /*<------------------ ^^ End of Account Registration ------------------>*/

    /*<------------------ Creating an Account ------------------>*/

    // "First Name" Field
    firstName(){
        return cy.get('[data-elid="first-name-input"]')
    }

    // "Last Name" Field
    lastName(){
        return cy.get('[data-elid="last-name-input"]')
    }

    // "Email Address" Field
    emailAddress(){
        return cy.get('[data-elid="email-input"]')
    }

    // "Confirm Email" Field
    confirmEmail(){
        return cy.get('[data-elid="confirm-email-input"]')
    }

    // "Passenger Type" Field
    passengerType(){
        return cy.get('[data-elid="passenger-type"')
    }

        // "Group Passenger" Option
        groupPassenger(){
            return cy.get('#react-select-3-option-3')
        }

    // "Mobile Number" Field
    mobileNum(){
        return cy.get('[data-elid="phone-number-input"]')
    }

    // "Password" Field
    password(){
        return cy.get('[data-elid="password-input"]')
    }

    // "Confirm Password" Field
    confirmPassword(){
        return cy.get('[data-elid="confirm-password-input"]')
    }

    // "Email" Checkbox
    emailCheckbox(){
        return cy.get('._1Pcgin_EhxL4yKxSdh07_7')
    }

    // "Create an Account" CTA
    createAccountCTA(){
        return cy.get('[data-elid="submit-button"]')
    }

    // "Getting started" CTA
    gettingStartedCTA(){
        return cy.get('._nfmPejYIgKBBPi4ONjx7._2CxCmKVUxIDVETuvcIv58b')
    }

    /*<------------------ ^^End of Creating an Account ------------------>*/

    /*<------------------ Logging Out ------------------>*/
    // My Arriva CTA in the Nav Bar
    myArrivaNav(){
        return cy.get('[data-elid="top-line-account-logged-in"]')
    }

        // My Arriva Log Out Tab
        logOut(){
            return cy.get('._1AMsuj8lTsiiNB5Nkc7P1S')
        }

        // Log Out popup
        logOutPopup(){
            return cy.get('._3DkMvrHp7_-vCwNW9O-IQG')
        }

            // Confirm CTA 
            logOutConfirm(){
                return cy.get('[data-elid="confirm-button"]')
            }
    
    /*<------------------ ^^End of Logging Out ------------------>*/

    logInNav(){
        // return cy.get('[data-elid="top-line-account-not-logged"]')
        return cy.get('._3bi8GZJiO3BfRq1vL9ld5A._34C8axSfDqh6B3np8MbZ2z')
    }

    /*<------------------ Negative Acount Creation ------------------>*/
    // Errors 
    createAccountRegErrors(){
        return cy.get('._3XdD3jQFJX5vl5yLd61ujs')
    }

    /*<------------------ ^^End of Negative Acount Creation ------------------>*/
}
export default elements