import homePage from "./homePage_POM";
import accountCreation from "./CreatingAnAccount_POM";
import logOut from "./LogOut_POM";

// Initialising POM Classes
const home = new homePage()
const createAccount = new accountCreation()
const loggingOut = new logOut()

// Disregarding uncaught exceptions
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
    });

describe('Account Registration Flow', () => {

    it('Launching the Preprod Website', () => {
      
        // Launching the Website
        home.preprodNavigation()
    
    })

    it('Traversing to the "Create an Account" Page', () => {

        // Traversing to the "Create an Account" Page from the Nav Bar
        home.createAnAccount()

    })

    it('Creating an Account', () => {

        // Filling in the "Create an Account" Form
        createAccount.successfulAccountCreation()

    })

    it('Logging Out', () => {

        // Logging out the User
        loggingOut.loggingOut()

    })

    it('Signing in Again', () =>{

        createAccount.newAccountSignIn()
    })

})