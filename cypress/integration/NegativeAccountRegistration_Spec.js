/// <reference types="cypress" />
import accountCreation from "./CreatingAnAccount_POM";
import homePage from "./homePage_POM";

// Initialising POM Classes
const home = new homePage()
const create = new accountCreation()


describe('Negative Scenarios for Account Registration', () => {

    it('Launching the Website', () => {

        // Launching the Website
        home.preprodNavigation()
    })

    it('Traversing to the "Create an Account" Page', () => {

        // Traversing to the "Create an Account" Page from the Nav Bar
        home.createAnAccount()

    })

    it('Leaving all the Fields Empty', () => {

        // Leaving all the field empty
        create.emptyFields()

    })

    it('Entering the wrong email Address', () => {

        // Wrong Email Address
        create.wrongEmailAddress()

    })

    it('Entering the wrong Passwords', () => {

        // Wrong Email Address
        create.wrongPasswords()
        
    })

    it('Entering incorrect Email Formats', () => {

        // Wrong Email Address
        create.incorrectEmail()
        
    })

    it('Entering incorrect Password Formats', () => {

        // Wrong Email Address
        create.incorrectPassword()
        
    })
})