/// <reference types="cypress" />
import homePage from "./homePage_POM"
import signIn from "./signIn_POM";
import elements from "./webElements"


// Initialising POM Classes
const signingIn = new signIn()

// Random Number Geenrator Function
function generateRandomInteger(min, max) {
    return Math.floor(min + Math.random()*(max + 1 - min))
  }

/*<--- Variable Declaration --->*/
// Saving the From Station Selected
var from_Station_name;

// Saving the To Station Selected
var to_Station_name;

// Saving the Date and Time Selected
var date_and_time;

var email = 'mustafa.waris+1111@ormdigital.com'
var password = 'Pa55w0rd001!'

class P2PPOM extends elements{

    journeySearch(from, to){

        // Typing in the From Station Text Box
        this.fromStation()
            .type(from, {force: true})

        // Waiting for the Dropdown to populate
        cy.wait(7000)

        // Calculating a Random number between the first and last element in the list and clicking on it
        this.Dropdown().its('length').then((number) => {
            
            // Running the Random Number function
            const randomNum = generateRandomInteger(0,number-1)

            // Clicking on the Random Number in the Dropdown
            this.Dropdown().eq(randomNum).click()

        })

        // Saving the Selected Stations Name
        this.selectedStations().then(($fromStation) => {

            // Storing the text selected from the Dropdown Item
            from_Station_name = $fromStation.text()
            // console.log('From Station: ' + from_Station_name)
            })
        

        // Typing in the To Station Text Box
        this.toStation()
            .type(to, { force: true})
        
        // Waiting for the Dropdown to populate
        cy.wait(7000)

        // Calculating a Random number between the first and last element in the list and clicking on it
        this.Dropdown().its('length').then((number) => {
            
            // Running the Random Number function
            const randomNum = generateRandomInteger(0,number-1)

            // Clicking on the Random Number in the Dropdown
            this.Dropdown().eq(randomNum).click()

        })

        // Saving the Selected Stations Name
        // eq(1) bcs the To Stations text box is in the second index
        this.selectedStations().eq(1).then(($toStation) => {

            // Storing the text selected from the Dropdown Item
            to_Station_name = $toStation.text()
            // console.log('To Station: ' + to_Station_name)
            })

        // Clicking the Date Pciker Text box
        this.datePicker().click()

        // Clicking Done on the Date Picker Dropdown
        this.datePickerDone().click()

        // Saving the Date and Time value
        this.dateAndTime().then(($dateAndTime) => {
            
            // Storing the Date and Time
            date_and_time = $dateAndTime.val()
        })
        // Clicking the "Plan my Journey" CTA
        this.planMyJourneyCTA().click()
    }

    /*<------------------------------ ^^End of Journey Search  ------------------------------>*/

    /*<------------------------------ Plan my Journey  ------------------------------>*/
    planMyJourneyCheck(){

        // Checking the "From Station"
        this.selectedStations().eq(0).should('have.text', from_Station_name)

        // Checking the "To Station"
        this.selectedStations().eq(1).should('have.text', to_Station_name)

        // Checking the Date and Time
        this.dateAndTime().should('have.value', date_and_time)

        // Checking that the Map is displayed
        this.planMyJourneyMap().should('be.visible')

        // Checking that the Journey list is populated
        this.journeyListItems().should('have.length.gt', 0)

        // Clicking the Later buses CTA
        this.laterBuses().click()

        // Checking that the Journey list is populated
        this.journeyListItems().should('have.length.gt', 5)

    }

    /*<------------------------------ ^^ End of Plan my Journey  ------------------------------>*/

    /*<------------------------------ Add Tickets  ------------------------------>*/
    addTickets(){

        // Clicking on the First Journey
        this.journeyListItems().eq(0)
            .click()

        cy.wait(2500)

        // Clicking on the "Buy Tickets" CTA
        this.buyTicketsCTA()
            .click()

        cy.wait(5000)

        // Adding and Adult Ticket
        this.adultTicket()
            .click()

        // Loading more Ticket Types
        this.moreTickets()
            .click()

        // Selecting the Child Section
        this.childSection()
            .click()

        // Adding a Child Week Ticket
        this.childWeekTicket()
            .click()

        // Clicking the Checkout CTA
        this.checkoutCTA()
            .click()

    }

    // Signing into an existing Account
    signingIn(){
        
        // Clicking the "Sign In" CTA in the Nav Bar
        this.signInCTA()
            .click()

        // Using the Email and Password defined above
        signingIn.signIn(email, password)

    }

    

}
export default P2PPOM