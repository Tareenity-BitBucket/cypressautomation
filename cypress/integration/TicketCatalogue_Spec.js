/// <reference types="cypress" />
import homePage from "./homePage_POM";
import ticketCat from "./TicketCatalogue_POM";
import signIn from "./signIn_POM";
import payment from "./Payment_POM";

// Disregarding uncaught exceptions
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
    });

describe('Ticket Catalogue Flow', () => {

    // Initialising the POM Classes
    const home = new homePage()
    const tickets = new ticketCat()
    const signingIn = new signIn()
    const paymentFlow = new payment()

    // Launching the Website
    it('Launching the Website', () => {

        home.preprodNavigation()

    })

    // Traversing to the "Buy Tickets Page"
    it('Traversing to the "Buy Tickets" Page', () => {

        home.buyTicketsPage()

    })

    // Checking that the Regions displayed are correct
    it('Checking that the Regions Loaded are correct', () => {

        tickets.regionListCheck()

    })

    // Checking that the Zones populate when a region is clicked
    it('Checking that Zones populate when a Region is clicked', () => {

        tickets.zoneCheck()

    })

    // Buying a Ticket
    it('Buying a Ticket from the "Beds and Bucks" Region', () => {

        tickets.buyingATicket()
        
    })

    // Signing in 
    it('Signing into an Account',() => {

        signingIn.signIn()

    })

    // Payment Flow 
    it('Checking out and Buying the Tickets', () => {

        paymentFlow.paymentFlow()
        
    })



})


