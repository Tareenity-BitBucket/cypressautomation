import accountCreationForm from "./accountFrom_POM";
import signIn from "./signIn_POM";
import elements from "./webElements";

// Initialising POM Classes
const signingIn = new signIn()
const accountForm = new accountCreationForm()

// Initialising the email Variable

// Errors
var emptyFirstNameError = 'Please enter your name'
var emptyLastNameError = 'Please enter your name'

var emptyEmailError = 'Please enter your email'
var emptyPasswordError = 'Please enter your password'

var wrongEmailError = 'The email addresses entered do not match'
var wrongPasswordError = 'The passwords entered do not match'

var incorrectEmailError = 'Must be a valid email'
var incorrectPasswordError = 'Passwords need at least 8 characters and include one uppercase, lowercase and numeric character and one of the following special characters @$!%*#?&'

// Field Names
const first_name = 'Ammad'
const last_name = 'Tareen'

var email = 'Ammad.Tareen' + Math.floor(Math.random() * 1000) + '@gmail.com';
const wrongEmail = 'Ammad.Tareen@gmail.com'
const inValidEmail = 'Ammad.Tareengmail'

const mobile_number = '07444151716'

const password = 'Pa55w0rd001!'
const wrongPassword = 'Pa55w0rd'
const inValidPassword = 'ppppppp'



class accountCreation extends elements {

    successfulAccountCreation(){

        // Filling out the Fields with Valid data
        accountForm.createAnAccountForm(first_name, last_name, email, email, mobile_number, password, password)
        
        // Clicking the "Get Started" CTA
        this.gettingStartedCTA()
            .click()
    }

    // Signing into the newly Created Account 
    newAccountSignIn(){

        // Clicking the "Log In" CTA in the Nav Bar
        this.logInNav()
            .click()
        // cy.contains('Log in').click()

        cy.wait(5000)

        // Using the Email and Password defined above
        signingIn.signIn(email, password)

    }

    // Negative Test - Empty Fields
    emptyFields(){

        // Clicking the "Create Account" CTA
        this.createAccountCTA()
            .click()

        // Checking the Errors

        // Frist Name Error
        this.createAccountRegErrors().eq(0)
            .should('have.text', emptyFirstNameError)

        // Last Name Error
        this.createAccountRegErrors().eq(1)
            .should('have.text', emptyLastNameError)

        // Email Error
        this.createAccountRegErrors().eq(2)
            .should('have.text', emptyEmailError)

        // Password Error
        this.createAccountRegErrors().eq(3)
            .should('have.text', emptyPasswordError)
    }

    wrongEmailAddress(){

        // Entering the wrong Email Addresses
        accountForm.createAnAccountForm(first_name, last_name, email, wrongEmail, mobile_number, password, password)

        // Checking the Error
        this.createAccountRegErrors()
            .should('have.text', wrongEmailError)

    }

    wrongPasswords(){

        // Entering the Wrong Passwords
        accountForm.createAnAccountForm(first_name, last_name, email, email, mobile_number, password, wrongPassword)

        // Checking the Error
        this.createAccountRegErrors()
            .should('have.text', wrongPasswordError)

    }

    incorrectEmail(){

        //Entering the Incorrect Email Format
        accountForm.createAnAccountForm(first_name, last_name, inValidEmail, inValidEmail, mobile_number, password, password)

        // Checking the Error
        this.createAccountRegErrors()
        .should('have.text', incorrectEmailError)
    }

    incorrectPassword(){

        //Entering the Incorrect Email Format
        accountForm.createAnAccountForm(first_name, last_name, email, email, mobile_number, inValidPassword, inValidPassword)

        // Checking the Error
        this.createAccountRegErrors()
        .should('have.text', incorrectPasswordError)

    }

}
export default accountCreation