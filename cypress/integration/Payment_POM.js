import elements from "./webElements";

class payment extends elements {

    // Checking out
    paymentFlow(){

        cy.wait(10000)

        // IF Condition
        this.cardAlreadyAdded().then(($span) => { 

            // Runs the IF Condititon
            if ($span.is(':visible')){

                // When there is a card saved
                console.log('Card available')
                
                // Clicking the "Confirm Payment" CTA
                this.confirmPaymentCTA()
                    .click()

            } else {

                // When there is no card saved
                console.log('No Card')

                // Selecting the Card Payment Option
                this.cardPaymentOption()
                    .click()
                
                // Card number input
                this.creditCardNum()
                    .type('4111111111111111')

                // Expiration date input
                this.creditCardexpirationDate()
                    .type('1225')

                // Entering the CVV
                this.creditCardCVV()
                    .type('111')

                // Entering the Post code
                this.creditCardPostal()
                    .type('E62AW')

                // Confirming Payment
                this.confirmPaymentCTA()
                    .click()

            }
          })

          cy.wait(7000)

        // Entering the OTP Number
        this.OTPNum()
            .clear()
            .type('1234')
    
        // Clicking the OTP Submit CTA
        this.OTPSubmitCTA()
            .click()

            cy.wait(10000)

        cy.get('.ZJIraHndglfcUgMDSia6k > div').should('have.text', 'Booking confirmed.Your payment was successful')
    }
}
export default payment