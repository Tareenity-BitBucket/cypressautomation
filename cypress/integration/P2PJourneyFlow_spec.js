/// <reference types="cypress" />
import homePage from "./homePage_POM";
import P2PPOM from "./P2PJourneyFlow_POM";
import signIn from "./signIn_POM";
import payment from "./Payment_POM";

// Disregarding uncaught exceptions
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
    });

describe('Running the P2P Journey Flow Smoke Test', () => {

    // Initialising the POM Classes to be used
    const P2P = new P2PPOM()
    const home = new homePage()
    const signingIn = new signIn()
    const paymentFlow = new payment()

    // Launching the Homepage
    it('Launching the Website', () => {
        home.preprodNavigation()
    })

    // Entering Data in the Journey Search Text Boxes
    it('Journey Search', () => {
        cy.wait(5000)
        P2P.journeySearch('Leicester', 'Leicester')
    })

    // Checking the Information displayed on the "Plan my Journey" Page
    it('Checking the Data presented on the Plan My Journey Page', () => {
        cy.wait(7000)
        P2P.planMyJourneyCheck()
    })

    // Adding an Adult Day & Child Week Ticket
    it('Adding Tickets', () => {
        P2P.addTickets()
    })

    // Signing In
    it('Signing In to an Account', () => {
        P2P.signingIn()
    })

    // Buying Tickets
    it('Checkout Flow', () => {
        paymentFlow.paymentFlow()
    })

})