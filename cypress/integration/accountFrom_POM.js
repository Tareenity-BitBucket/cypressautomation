import elements from "./webElements";

class accountCreationForm extends elements{

    createAnAccountForm(first_name, last_name, email1, email2, mobile_number, password1, password2){

        // Entering Data into the "First Name" Field
        this.firstName()
            .clear()
            .type(first_name)
        
        // Entering Data into the "Last Name" Field
        this.lastName()
            .clear()
            .type(last_name)

        // Entering Data into the "Email Address" Field
        this.emailAddress()
            .clear()
            .type(email1)

        // Entering Data into the "Confirm Email Address" Field
        this.confirmEmail()
            .clear()
            .type(email2)
        
        // Selecting a "Passenger Type"
        this.passengerType()
            .click()

            // Selecting the "Group" Passenger Type
            this.groupPassenger()
                .click()

        // Entering Data into the "Mobile Number"
        this.mobileNum()
            .clear()
            .type(mobile_number)

        // Entering Data into the "Password" Field
        this.password()
            .clear()
            .type(password1)

        // Entering Data into the "Confirm Password" Field
        this.confirmPassword()
            .clear()
            .type(password2)

        // Selecting the Email Checkbox
        this.emailCheckbox()
            .click()

        // Clicking the "Create Account" CTA
        this.createAccountCTA()
            .click()

    }
}
export default accountCreationForm