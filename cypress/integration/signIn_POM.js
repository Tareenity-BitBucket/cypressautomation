/// <reference types="cypress" />
import elements from "./webElements";

class signIn extends elements{

    // Sign In Function
    signIn(email, password){

        // Entering an Email Address
        this.signInEmail()
            .type(email)

        // Sign in Password
        this.signInPassword()
            .type(password)

        // Clicking the "Sign In" CTA
        this.signInCTA()
            .click()

        cy.wait(10000)

        // Clicking the "Continue" CTA
        this.signInContinueCTA()
            .click()
    }
    
}
export default signIn