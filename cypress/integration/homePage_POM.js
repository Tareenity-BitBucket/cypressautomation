/// <reference types="cypress" />
import elements from "./webElements";

class homePage extends elements {

    // Navigating to the Preprod URL and Clicking the Consent Popup
    preprodNavigation(){
        
        // Navigating to the Preprod Site
        this.preprodURL()

        // Clicking the Popup
        this.consentPopup().click()
    }

    // Traversing to the "Buy Tickets" Page
    buyTicketsPage(){

        // Clicking the "Tickets" heading in the Nav Bar
        this.ticketsHeading()
            .click()

        // Clicking the "Buy Tickets" section in the Nav Bar
        this.buyTicketsHeading()
            .click()
    }

    // Traversing to the "Create an Account" Page
    createAnAccount(){

        // Clicking the "Sign In" CTA in the Nav Bar
        this.signInNav()
            .click()

        // Create Account Tab on the Sign in page
        this.createAccountTab()
            .click()

    }


}
export default homePage