import elements from "./webElements";

class logOut extends elements {

    loggingOut(){

        // Clicking the "My Arriva" CTA in the Nav Bar
        this.myArrivaNav()
            .click()
            
        // Clicking the "Log Out" Tab
        this.logOut()
            .click()

        // Checking that the "Log Out" Popup Exists
        this.logOutPopup()
            .should('exist')

        cy.wait(2000)

        // Clicking the "Confirm" CTA on the Popup
        this.logOutConfirm()
            .click()
        
    }
}
export default logOut