import elements from "./webElements";

// Initialising Variables
var regionNames = ['Beds and Bucks', 'Greenline', 'Herts and Essex', 'Kent and Surrey', 'Midlands', 'North East', 'North West', 'Wales', 'Yorkshire']

class ticketCat extends elements{

    regionListCheck(){

        // Checking that 9 regions are displayed
        this.regionList().should('have.length', 9)

        // Checking 'Beds and Bucks'
        this.regionList().eq(0)
            .should('have.text', regionNames[0])

        // Checking 'Greenline'
        this.regionList().eq(1)
            .should('have.text', regionNames[1])

        // Checking 'Herts and Essex'
        this.regionList().eq(2)
            .should('have.text', regionNames[2])

        // Checking 'Kent and Surrey'
        this.regionList().eq(3)
            .should('have.text', regionNames[3])
        
        // Checking 'Midlands'
        this.regionList().eq(4)
            .should('have.text', regionNames[4])

        // Checking 'North East'
        this.regionList().eq(5)
            .should('have.text', regionNames[5])

        // Checking 'North West'
        this.regionList().eq(6)
            .should('have.text', regionNames[6])

        // Checking 'Wales'
        this.regionList().eq(7)
            .should('have.text', regionNames[7])
        
        // Checking 'Yorkshire'
        this.regionList().eq(8)
            .should('have.text', regionNames[8])
    }

    zoneCheck(){

        // 1. Beds and Bucks
        this.regionSelection().eq(0).click()
            this.regionHeading().should('have.text', 'Buy bus tickets in '+regionNames[0])

            // Checking that the Zones populate
            this.zoneList().should('have.length.gt', 0)

        // Change the Region
        this.backToRegionsPage()
            .click()

        /* <------------------------------------------------------> */

        // 2. Greenline
        this.regionSelection().eq(1).click()
            this.regionHeading().should('have.text', 'Buy bus tickets in '+regionNames[1])

            // Checking that the Zones populate
            this.zoneList().should('have.length.gt', 0)

        // Change the Region
        this.backToRegionsPage()
            .click()

        /* <------------------------------------------------------> */

        // 3. Herts and Essex
        this.regionSelection().eq(2).click()
            this.regionHeading().should('have.text', 'Buy bus tickets in '+regionNames[2])

            // Checking that the Zones populate
            this.zoneList().should('have.length.gt', 0)

        // Change the Region
        this.backToRegionsPage()
            .click()

        /* <------------------------------------------------------> */

        // 4. Kent and Surrey
        this.regionSelection().eq(3).click()
            this.regionHeading().should('have.text', 'Buy bus tickets in '+regionNames[3])

            // Checking that the Zones populate
            this.zoneList().should('have.length.gt', 0)

        // Change the Region
        this.backToRegionsPage()
            .click()

        /* <------------------------------------------------------> */

        // 5. Midlands
        this.regionSelection().eq(4).click()
            this.regionHeading().should('have.text', 'Buy bus tickets in '+regionNames[4])

            // Checking that the Zones populate
            this.zoneList().should('have.length.gt', 0)

        // Change the Region
        this.backToRegionsPage()
            .click()

        /* <------------------------------------------------------> */

        // 6. North East
        this.regionSelection().eq(5).click()
            this.regionHeading().should('have.text', 'Buy bus tickets in '+regionNames[5])

            // Checking that the Zones populate
            this.zoneList().should('have.length.gt', 0)

        // Change the Region
        this.backToRegionsPage()
            .click()

        /* <------------------------------------------------------> */

        // 7. North West
        this.regionSelection().eq(6).click()
            this.regionHeading().should('have.text', 'Buy bus tickets in '+regionNames[6])

            // Checking that the Zones populate
            this.zoneList().should('have.length.gt', 0)

        // Change the Region
        this.backToRegionsPage()
            .click()

        /* <------------------------------------------------------> */

        // 8. Wales
        this.regionSelection().eq(7).click()
            this.regionHeading().should('have.text', 'Buy bus tickets in '+regionNames[7])

            // Checking that the Zones populate
            this.zoneList().should('have.length.gt', 0)

        // Change the Region
        this.backToRegionsPage()
            .click()

        /* <------------------------------------------------------> */

        // 9. Yorkshire
        this.regionSelection().eq(8).click()
            this.regionHeading().should('have.text', 'Buy bus tickets in '+regionNames[8])

            // Checking that the Zones populate
            this.zoneList().should('have.length.gt', 0)

        // Change the Region
        this.backToRegionsPage()
            .click()

    }

    buyingATicket(){
     
        // Buying a ticket for the "Beds and Bucks" Region
        this.regionSelection().eq(0).click()

        // Selecting a Zone
        this.zoneSelection().eq(1).click()

        cy.wait(5000)

        // Selecting the Child Section
        this.childSection()
            .click()

        // Adding a Child Week Ticket
        this.childWeekTicket()
            .click()

        // Clicking the Checkout CTA
        this.ticketCheckoutCTA()
            .click()


    }
}
export default ticketCat